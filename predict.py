
import numpy as np

import tensorflow as tf
from tensorflow import keras
from keras.utils import plot_model

import matplotlib.pyplot as plt
from scipy.signal import argrelmax
from PIL import Image
import cv2


directory = '/media/paul/My Passport/paul/project_DNN/data/goo/'
ground_truth = 'ot_2_2015_07_16_a.txt'
model_name = 'big_2conv8FC161class_id_1_rand-8-4_375_360_crop_drop aug.h5'
model_path = 'use/'+ model_name 
save = True

item_depth = 1
window = 16     # amount of examples to predict on
x_dim = 585
y_dim = 580
frame_rate = 1 / 5.0

x_dim_crop = 375
y_dim_crop = 360

y_off = y_dim - y_dim_crop
x_off = x_dim - x_dim_crop

item = np.ndarray(shape=(window, x_dim_crop, y_dim_crop, item_depth), dtype='float32')

# load model
model = keras.models.load_model(model_path)
print(model.summary())
# SVG(model_to_dot(model).create(prog='dot', format='svg'))
# plot_model(model, to_file='model.png')

# read in information about videos
td 	   = open(directory + ground_truth, "r")
lines  = td.readlines()
videos = len(lines) - 1


def get_samples(cap, window, frames):

    item = np.ndarray(shape=(window, x_dim_crop, y_dim_crop, frames), dtype='float32')
    for w in range(window):
       for f in range(0,frames):
        ret, frame = cap.read()
        if ret == True:
            item[w,:,:,f] = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)[x_off:x_dim, 0:y_dim_crop]            
        else:
            print('end')
            return ret, -1
    return ret, item

def get_first_event(y_pred):
    window = len(y_pred)
    for w in range(window):
        if y_pred[w] > 0.7:
            return w
    return -1

def get_max(y_pred):
    window = len(y_pred)
    maxi = 0
    ev = 0
    for w in range(window):
        if y_pred[w] > maxi:
            maxi = y_pred[w]
            ev = w
    return maxi, ev

def pred_video_first(cap, window):
    k=0
    yy = []
    while True:    
        ret, frames = get_samples(cap, window, item_depth)
        if ret == True:
            norm = np.max(frames)
            frames /= norm
#                
            y_pred = model.predict(frames)
            yy.append(y_pred)
            ev = get_first_event(y_pred)
            event_pred = k*window*item_depth + ev
            if ev != -1:
                shape = [len(yy)*window]
                yy_arr = np.concatenate(yy).reshape(shape)
                return event_pred, yy_arr
        else:
            shape = [len(yy)*window]
            yy_arr = np.concatenate(yy).reshape(shape)
            return -1, yy_arr

        k = k+1

def pred_video_max(cap, window):
    k=0
    maxi = 0
    yy = []
    while True:    
        ret, frames = get_samples(cap, window, item_depth)
        if ret == True:
            norm = np.max(frames)
            frames /= norm
            
            y_pred = model.predict(frames)
            yy.append(y_pred)
            temp, ev = get_max(y_pred)
            if temp > maxi:
                maxi = temp
                max_event = k*window*item_depth + ev
        else:            
            shape = [len(yy)*window]
            yy_arr = np.concatenate(yy).reshape(shape)
            return max_event, maxi , yy_arr

        k = k+1

def pred_video(cap, window):
    k=0
    yy = []
    while True:    
        ret, frames = get_samples(cap, window, item_depth)
        if ret == True:
            
            mean = np.mean(frames)
            frames /= mean
            
            y_pred = model.predict(frames)
            yy.append(y_pred)
        else:
            shape = [len(yy)*window]
            yy_arr = np.concatenate(yy).reshape(shape)
            
            return yy_arr, argrelmax(yy_arr, order = 400)

        k = k+1
        
i=0
dif = []
for line in lines:
        
    # omit first line  
    if(i>255):

        # extract file information                
        info = line.split('\t')
        print('reading video %d of %d' % (i, videos))
        video_name = info[0]
        event_time = int(info[1])

        # open video
        cap = cv2.VideoCapture(directory + video_name)
        if(cap.isOpened()==False):
            print("couldn't open video"+ video_name)
            exit()

        print ('event real @ %d' % event_time )
        event_frame = int(event_time * frame_rate)

        # predict local maxima
#        y_pred, extr = pred_video(cap, window)
#        event_pred = extr[0][0]

        # predict max conduct
        #event_pred, maxi, y_pred = pred_video_max(cap, window)
        
        # predict first conduct
        event_pred, y_pred = pred_video_first(cap, window)

        diff = event_time - event_pred * 5
        dif.append(diff)
        #print('maximum: ' , maxi)
        print('event pred @ %d' % (event_pred * 5))
        print('diff %d ms' % diff)

        # save images
        if save == True:
            cap.set(1, event_pred)
            ret, frames = get_samples(cap, 1, item_depth)
            for f in range(item_depth):
                img = Image.fromarray(frames[0,:,:,f].astype('uint8'))

            cap.set(1, event_frame)
            ret, frames = get_samples(cap, 1, item_depth)
            for f in range(item_depth):
                img2 = Image.fromarray(frames[0,:,:,f].astype('uint8'))

            #numpy_vertical = np.vstack((image, grey_3_channel))
            numpy_horizontal = np.hstack((img, img2))
            imgn = Image.fromarray(numpy_horizontal.astype('uint8'))
            #numpy_vertical_concat = np.concatenate((image, grey_3_channel), axis=0)
        # numpy_horizontal_concat = np.concatenate((image, grey_3_channel), axis=1)
            
            title = 'pics/video' + str(i) + 'diff' + str(diff) + '.png'
            imgn.save(title)
#
    i = i+1

dif = np.asarray(dif)
avg = sum(dif)/len(dif)
difsqsum = sum(np.power(dif,2))
sqrerr = np.sqrt(difsqsum)
std = np.std(dif)
print ('sqreer: ', sqrerr)
print ('avg: ', avg)
print ('std: ', std)
print ('min: ', min(dif))
print ('max: ', max(dif))
fig1 = plt.hist(dif, bins=10, range=(-100, 100))
plt.xlabel("difference [ms]")
plt.ylabel("Frequency")
plt.savefig('histos/Histogram_' + model_name + '-100_100' + '.png')
plt.show()
fig2 = plt.hist(dif, bins=10)
plt.xlabel("difference [ms]")
plt.ylabel("Frequency")
plt.savefig('histos/Histogram_' + model_name + 'auto' + '.png')