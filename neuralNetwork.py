# %% init

from __future__ import print_function

import numpy as np
from random import randint
    
import keras

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Conv3D, MaxPooling3D
from keras import backend as K

#from IPython.display import SVG
#from keras.utils.vis_utils import model_to_dot
#from keras.utils import plot_model

# %% go 

# inputs
dataset = '4items_id_1_rand-8-4_64_64_crop_'
x_path = 'datasets/' + dataset + 'x.npy'
y_path = 'datasets/' + dataset + 'y.npy'

model_type = 'small'
filters = 16
fc_neur = 16
fc_neur2 = 16
desc = '_2conv' + str(filters) + 'FC' + str(fc_neur)  
model_name =  model_type + desc + dataset  + 'aug' + '.h5'
model_path = 'models/'+ model_name 

batch_size = 32
epochs = 256

# %% data set
def get_data_set(x_path, y_path):
    x = np.load(x_path)
    y = np.load(y_path)
    
    tot_items = len(x)
    train_items = int(0.8 * tot_items)
    
    x_train = x[0:train_items,:,:,:]
    y_train = y[0:train_items]
    
    x_test = x[train_items:tot_items,:,:,:]
    y_test = y[train_items:tot_items]
    
    return x_train, y_train, x_test, y_test 

    

print('loading data set...')
x_train, y_train, x_test, y_test = get_data_set(x_path, y_path)
print('done')

input_shape = x_train[0,:,:,:].shape
print ('input shape:', str(input_shape))


norm = np.max(x_train)
x_train /= norm
x_test /= norm

datagen = ImageDataGenerator(
#    featurewise_center=True,
#    featurewise_std_normalization=True,
    rotation_range=5,
    width_shift_range=0.1,
    height_shift_range=0.1,
#    horizontal_flip=True
)
datagen.fit(x_train)

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')


# %% neural network

if model_type == 'big':

    model = Sequential()
    model.add(Conv2D(1 * filters, kernel_size=(3, 3),
                    activation='relu',
                    input_shape=input_shape))
    model.add(Conv2D(1 * filters, kernel_size=(3, 3),
                    strides=2,
                    activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    activation='relu'))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    strides=2,
                    activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    activation='relu'))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    activation='relu'))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    strides=2,
                    activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
   # model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(fc_neur, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(fc_neur, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(1, activation='sigmoid'))
   

elif model_type == 'small':
    model = Sequential()
    model.add(Conv2D(1 * filters, kernel_size=(3, 3),
                    activation='relu',
                    input_shape=input_shape))
    model.add(Conv2D(1 * filters, kernel_size=(3, 3),
                    strides=2,
                    activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3)))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    activation='relu',
                    input_shape=input_shape))
    model.add(Conv2D(2 * filters, kernel_size=(3, 3),
                    strides=2,
                    activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3)))
    model.add(Flatten())
    model.add(Dense(fc_neur, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(fc_neur2, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(1, activation='sigmoid'))
    

print(model.summary())

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

#history_callback = model.fit(x_train, y_train,
#           batch_size=batch_size,
#           epochs=epochs,
#           verbose=1,    
#           validation_data=(x_test, y_test))
history_callback = model.fit_generator(datagen.flow(x_train, y_train, batch_size=32),
                    steps_per_epoch=len(x_train) / 32, epochs=epochs)

loss_history = history_callback.history["loss"]

numpy_loss_history = np.array(loss_history)
np.savetxt("loss_history" + model_name + ".txt", numpy_loss_history, delimiter=",")


score = model.evaluate(x_test, y_test, verbose=0)

print('Test loss:', score[0])
print('Test accuracy:', score[1])

y_pred = model.predict(x_test)

model.save(model_path)