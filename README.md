# README #

These python scripts were created in order to detect whisker touch events in behavorial mice videos.

Using the scripts one can:
	+ create a data set
	+ train a model
	+ let the model predict touch events in mice videos

we seperated the scripts for creating data sets with small pictures and predicting events on a model that uses small pictures from those that use high resolution pictures, which might be changed in the future.

IMPORTANT NOTE: There are no videos  available for a public submission.
