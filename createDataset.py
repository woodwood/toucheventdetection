# %% init

from __future__ import print_function

import numpy as np
from random import randint
import cv2
    
# %% go 

# inputs
#directory = '/home/paul/Projects/data/a/'
directory = '/media/paul/My Passport/paul/project_DNN/data/goo/'
ground_truth = 'ot_2_2015_07_16_a.txt'
datasets = '/home/paul/Projects/gitzz/machinelearning/datasets/'
x_loc = '10items1class_id_1_rand-5--1_375_360_crop_x.npy'
y_loc = '10items1class_id_1_rand-5--1_375_360_crop_y.npy'

frame_rate = 1 / 5.0
x_dim = 585
y_dim = 580
item_depth = 1

x_dim_crop = 375
y_dim_crop = 360
y_off = y_dim - y_dim_crop
x_off = x_dim - x_dim_crop


# false_frames = [-256, -64, -8, -4]
# true_frames =  [ 0, item_depth, 2*item_depth, 3*item_depth]

false_frames = [-256, -64, -8, -2. -5, -5, -4, -3, -2, -1]
true_frames =  [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

get_rand = True     # get random samples?
smpls_per_vid = len(true_frames)
static_vids = 5

# read all lines from the file of ground truth
tf 	  = open(directory + ground_truth, "r")
lines = tf.readlines()

videos 	  = len(lines) - 1
samples	  = len(false_frames) + len(true_frames) 
examples  = videos*samples

x = np.ndarray(shape=(examples, x_dim_crop, y_dim_crop, item_depth), dtype='float32')
y = np.ndarray(shape=(examples), dtype='int')

def get_frames(cap, index, frames):
    cap.set(1,index)
    item = np.ndarray(shape=(x_dim_crop, y_dim_crop, frames), dtype='float32') 
    for i in range(0,frames):
        ret, frame = cap.read()
        if ret == True:
            item[:,:,i] = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)[x_off:x_dim, 0:y_dim_crop]      
        else:
            print('error while reading')
            return -1

    return item.copy()

# parse line by line
i=0
for line in lines:
    
    # omit first line  
    if(i>0):

        print('reading video %d of %d' % (i,videos))
        
        # extract file information
        info = line.split('\t')
        video_name = info[0]
        event_time = int(info[1])
    
        # calculate frame number of event from its time information
        event_frame = int(event_time * frame_rate)
  
        # open video
        cap = cv2.VideoCapture(directory + video_name)
        if(cap.isOpened()==False):
            i = i+1
            print("couldn't open video "+ video_name)
            continue

        if get_rand == True:
            for l in range(smpls_per_vid - static_vids):
                false_frames[l] = np.random.randint(-event_frame,-item_depth)

        # get n examples of class false
        k = 0
        for f in false_frames:
            ind = (i-1) * (samples) + k
            x[ind,:,:,:] = get_frames(cap, event_frame + f, item_depth)
            y[ind]	 = 0
            k = k+1
            
        # get n examples of class true
        k = 0
        for t in true_frames:
            ind = (i-1) * (samples) + k + len(false_frames)
            x[ind,:,:,:] = get_frames(cap, event_frame + t, item_depth)
            y[ind]     = 1
            k = k+1

    i = i+1

# output format
# x = (items, x_dim, y_dim, depth)
# y = (items, thruth[2])

np.save(datasets + x_loc, x)
np.save(datasets + y_loc, y)